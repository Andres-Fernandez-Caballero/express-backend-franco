const express = require("express");
const AdministradorEstudiantes = require("./administradorEstudiantes");
const app = express();

// nos permite enviar informacion por medio de los metodos post, put y delete
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.set("view engine", "ejs");
app.set("views", "./src/views");
// set a public folder
app.use(express.static("public"));

app.get("/", function (req, res) {
  res.render("index");
});

app.post("/", function (req, res) {
  const infrormacionCliente = req.body; // body devuelve un objeto de datos con informacion
  res.send(
    `<h1>Bienvenido ${infrormacionCliente.nombre} ${infrormacionCliente.apellido}`
  );
});

app.get("/formulario/saludo", function (req, res) {
  res.send(`
    <h1>Formulario de saludo</h1>
    <p>complete el formulario para ser saludado</p>
    <form method="POST" action="/" >
        <input name="nombre" placeholder="nombre">
        <input name="apellido" placeholder="apellido">
        <button type="submit">Enviar</button>
    </form>
    `);
});

// parte API regresa informacion en formato JSON
// REST API
app.get("/api/v1/estudiantes", function (req, res) {
  const estudiantes = AdministradorEstudiantes.listar();
  return res.status(200).json({
    code: 200,
    message: "estudents",
    studentsCount: estudiantes.length,
    result: estudiantes,
  });
});

app.get("/api/v1/estudiantes/:id", function (req, res) {
  const id = req.params.id;
  const estudiante = AdministradorEstudiantes.buscar(id);

  if (estudiante) {
    return res.status(200).json({
      code: 200,
      message: "user found",
      result: estudiante,
    });
  } else {
    return res.status(400).json({
      code: 400,
      message: "user NOT found 🫤",
    });
  }
});



app.listen(3000, function () {
  console.log("servidor escuchando en el puerto http://localhost:3000");
});
