const fs = require('fs');
/*
    Esta funcion utilizara el gestor de archivos para guardar la informacion de un estudiante
*/
const ruta_archivo = './estudiantes.json';
const AdministradorEstudiantes = {
    agregar: (estudiante) =>{
        let estudiantes = fs.readFileSync(ruta_archivo, 'utf-8');
        estudiantes = JSON.parse(estudiantes);
        const lastId = estudiantes[(estudiantes.length -1)].id
        const nuevoEstudiante = {id: (lastId +1), ...estudiante}
        estudiantes.push(nuevoEstudiante);
        fs.writeFileSync(ruta_archivo, JSON.stringify(estudiantes));
        return nuevoEstudiante
    },
    eliminar: (idestudiante) =>{
        let estudiantes = fs.readFileSync(ruta_archivo, 'utf-8')
        estudiantes = JSON.parse(estudiantes)
        const estudianteEliminado = estudiantes.find(estudiante => estudiante.id == idestudiante)
        if(estudianteEliminado){
            estudiantes = estudiantes.filter(estudiante => estudiante.id != idestudiante )
            fs.writeFileSync(ruta_archivo, JSON.stringify(estudiantes))
            return estudianteEliminado
        }else{
            return null
        }
    },
    modificar: (idestudiante, estudiante) =>{
        /**
         * @var estudiante:[{id:1, nombre:'Juan', apellido:'Perez', edad: 20}]
         */
        let estudiantes = fs.readFileSync(ruta_archivo, 'utf-8')
        estudiantes = JSON.parse(estudiantes)
        
        let estudianteModificado = estudiantes.find(e => e.id == idestudiante)
        console.log(estudianteModificado);
        if(estudianteModificado){
            estudianteModificado = {...estudianteModificado, ...estudiante}
            estudiantes = estudiantes.map(e => e.id == idestudiante ? estudianteModificado : e)
            fs.writeFileSync(ruta_archivo, JSON.stringify(estudiantes))
        }
        return estudianteModificado
        
    },
    listar: () =>{
        const estudiantes = fs.readFileSync(ruta_archivo, 'utf-8');
        return JSON.parse(estudiantes);
    },
    buscar: (idestudiante) =>{
        let estudiantes = fs.readFileSync(ruta_archivo, 'utf-8');
        estudiantes = JSON.parse(estudiantes);
        return estudiantes.find(estudiante => estudiante.id == idestudiante);
    }
}
module.exports = AdministradorEstudiantes;